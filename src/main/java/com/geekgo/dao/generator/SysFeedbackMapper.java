package com.geekgo.dao.generator;

import com.geekgo.core.base.BaseMapper;
import com.geekgo.model.generator.SysFeedback;

/**
 *由Mybatis Generator 工具自动生成,切勿随意修改
 */
public interface SysFeedbackMapper extends BaseMapper<SysFeedback> {
}