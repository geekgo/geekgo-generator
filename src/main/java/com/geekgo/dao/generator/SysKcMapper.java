package com.geekgo.dao.generator;

import com.geekgo.core.base.BaseMapper;
import com.geekgo.model.generator.SysKc;

/**
 *由Mybatis Generator 工具自动生成,切勿随意修改
 */
public interface SysKcMapper extends BaseMapper<SysKc> {
}