package com.geekgo.core.base;

import java.util.List;

/**
 * @author HeHaoyang
 * @version 2016/6/25 17:31
 */
public interface BaseMapper<T extends BaseModel> {
    List<T> selectAll();

    int deleteByPrimaryKey(Integer id);

    T selectByPrimaryKey(Integer id);

    int insert(T record);

    int updateByPrimaryKey(T record);
}
