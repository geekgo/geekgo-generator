package com.geekgo.core.generator.plugin;

import org.mybatis.generator.api.GeneratedJavaFile;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.JavaFormatter;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.api.dom.java.CompilationUnit;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.java.Interface;
import org.mybatis.generator.api.dom.java.JavaVisibility;
import org.mybatis.generator.internal.util.StringUtility;

import java.util.ArrayList;
import java.util.List;

/**
 * 生成mapper接口
 * @author HeHaoyang
 * @version 2016/6/24 16:06
 */
public class MapperPlugin extends PluginAdapter {

    private final String DEFAULT_DAO_SUPER_CLASS="com.geekgo.core.base.BaseMapper";
    private String daoTargetDir;
    private String daoTargetPackage;
    private String daoSuperClass;

    public MapperPlugin() {

    }

    @Override
    public boolean validate(List<String> list) {
        daoTargetDir = properties.getProperty("targetProject");
        boolean hasDir = StringUtility.stringHasValue(daoTargetDir);

        daoTargetPackage = properties.getProperty("targetPackage");
        boolean hasPackage = StringUtility.stringHasValue(daoTargetDir);

        daoSuperClass = properties.getProperty("daoSuperClass");
        if(!StringUtility.stringHasValue(daoSuperClass)){
            daoSuperClass = DEFAULT_DAO_SUPER_CLASS;
        }
        return hasDir&&hasPackage;
    }

    @Override
    public List<GeneratedJavaFile> contextGenerateAdditionalJavaFiles(IntrospectedTable introspectedTable) {

        JavaFormatter javaFormatter = context.getJavaFormatter();
        List<GeneratedJavaFile> mapperJavaFiles = new ArrayList<GeneratedJavaFile>();
        for (GeneratedJavaFile javaFile:introspectedTable.getGeneratedJavaFiles()){
            CompilationUnit unit = javaFile.getCompilationUnit();
            FullyQualifiedJavaType baseModelJavaType = unit.getType();
            String shortName = baseModelJavaType.getShortName();
            if(shortName.endsWith("Mapper")){
                continue;
            }
            Interface mapperInterface = new Interface(daoTargetPackage+"."+shortName+"Mapper");
            mapperInterface.setVisibility(JavaVisibility.PUBLIC);
            mapperInterface.addJavaDocLine("/**");
            mapperInterface.addJavaDocLine(" *由Mybatis Generator 工具自动生成,切勿随意修改");
            mapperInterface.addJavaDocLine(" */");

            FullyQualifiedJavaType daoSuperType = new FullyQualifiedJavaType(daoSuperClass);
            //添加泛型支持
            daoSuperType.addTypeArgument(baseModelJavaType);
            mapperInterface.addImportedType(baseModelJavaType);
            mapperInterface.addImportedType(daoSuperType);
            mapperInterface.addSuperInterface(daoSuperType);

            GeneratedJavaFile mapperJavaFile = new GeneratedJavaFile(mapperInterface,daoTargetDir,javaFormatter);
            mapperJavaFiles.add(mapperJavaFile);
        }
        return mapperJavaFiles;
    }
}
