/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50627
Source Host           : 127.0.0.1:3306
Source Database       : kitchen

Target Server Type    : MYSQL
Target Server Version : 50627
File Encoding         : 65001

Date: 2016-06-25 17:39:10
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_feedback
-- ----------------------------
DROP TABLE IF EXISTS `sys_feedback`;
CREATE TABLE `sys_feedback` (
  `id_` int(11) NOT NULL AUTO_INCREMENT,
  `name_` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_` text COLLATE utf8mb4_unicode_ci,
  `remark_` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enable_` int(1) NOT NULL DEFAULT '1',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `create_by` int(11) DEFAULT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of sys_feedback
-- ----------------------------

-- ----------------------------
-- Table structure for sys_kc
-- ----------------------------
DROP TABLE IF EXISTS `sys_kc`;
CREATE TABLE `sys_kc` (
  `id_` int(20) NOT NULL AUTO_INCREMENT,
  `kw_id` int(11) NOT NULL,
  `kc_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kc_avatar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kc_desc` text COLLATE utf8mb4_unicode_ci,
  `remark_` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enable_` int(1) NOT NULL DEFAULT '1',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `create_by` int(11) DEFAULT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of sys_kc
-- ----------------------------

-- ----------------------------
-- Table structure for sys_kw
-- ----------------------------
DROP TABLE IF EXISTS `sys_kw`;
CREATE TABLE `sys_kw` (
  `id_` int(11) NOT NULL AUTO_INCREMENT,
  `kw_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remark_` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enable_` int(1) NOT NULL DEFAULT '1',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `create_by` int(11) DEFAULT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of sys_kw
-- ----------------------------
